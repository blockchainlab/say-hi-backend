pragma solidity ^0.4.15;

/**
 * @title This is a sample contract for learning Ethereum and Solidity.
 * @author Scotiabank DF Blockchain Team
 */

contract HelloEthereum {

    /* Events - START */
    // Indexed arguments in a event are made filterable in the user interface.
    // Only up to 3 indexed arguments are allowed.
    event PersonRegistered(address indexed account, string name);
    event HiEthereum(address indexed account, string name);
    /* Events - END */

    /* Enum & Strut - START */
    enum Gender {
        Male,
        Female,
        Undisclosed
    }

    struct Person {
        string name;
        address account;
        Gender gender;
        uint age;
        bool registered;
        uint lastTimeSaidHi;
    }
    /* Enum & Strut - END */

    /* State Variables - START */
    // Constant state variable that cannot be changed later.
    uint constant SAY_HI_LOCK_DURATION = 10 seconds;

    // Mapping from addresses to struct Person.
    mapping (address => Person) public people;
    /* State Variables - END */

    /* Modifiers - START */
    modifier onlyRegisteredPerson { 
        require(people[msg.sender].registered);
        _;
    }

    modifier onlyEligiblePerson { 
        require(people[msg.sender].lastTimeSaidHi + SAY_HI_LOCK_DURATION <= now);
        _; 
    }

    // Function arguments can be passed into a modifier, _gender in this case.
    modifier onlyValidGender(uint _gender) { 
        require(uint(Gender.Undisclosed) >= _gender);
        _;
    }
    /* Modifiers - END */

    /* Constructor - START */
    function HelloEthereum () public {
        // empty constructor
    }
    /* Constructor - END */

    /* Public Functions - START */
    /** @dev Register a new person. Only accessible by the mainDev.
      *
      * @param _name Name of the person.
      * ...
      * @param _gender Gender of the person. 0 - Male, 1 - Female, 2 - Undisclosed, all other values will not be accepted.
      * ...
      *
      * @return true if execution succeeded, false otherwise.
      */
    function register(string _name, uint _gender, uint _age) onlyValidGender(_gender) public returns(bool) {
        // Set each individual field in the target Person struct. Since people is
        // a mapping from addresses to Person, the struct needs not to be manually
        // initialized.
        people[msg.sender].name = _name;
        people[msg.sender].gender = Gender(_gender);
        people[msg.sender].age = _age;
        people[msg.sender].registered = true;
        people[msg.sender].account = msg.sender;

        PersonRegistered(msg.sender, _name);

        return true;
    }

    /** @dev Say hi to ethereum. Only accessible by registered people and can only be called once by the same person in every 10 seconds.
      *
      * @return true if execution succeeded, false otherwise.
      */
    // When multiple modifiers are put onto the same function, they are called in sequence 
    // and then the actual function codes get run.
    function sayHi() onlyRegisteredPerson onlyEligiblePerson public returns(bool) {
        // Calling a internal function.
        updateSayHiTime();

        HiEthereum(msg.sender, people[msg.sender].name);

        return true;
    }

    /** @return true if the given address is registered, false otherwise.
      */
    function isPersonRegistered(address _account) public constant returns(bool) {
        return people[_account].registered;
    }
    /* Constant Function - END */
    /* Public Functions - END */

    /* Internal Functions - START */
    function updateSayHiTime() internal {
        people[msg.sender].lastTimeSaidHi = now;
    }

    function isContract(address _addr) internal constant returns (bool) {
        uint _size;
        assembly { _size := extcodesize(_addr) }
        return _size > 0;
    }
    /* Internal Functions - END */
}
